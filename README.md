Application to check whether a given IBAN is valid.

The root directory contains a IBANTester.jar file, which runs the application via command line.

To run the interactive IBAN checker, run the jar file without providing any additional arugments.

To run the file IBAN checker, run the jar with the first argument as the filename provided.