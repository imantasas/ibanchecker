import java.math.BigInteger;
import java.util.Arrays;

public class IBAN {

    private final String iban;
    private final BigInteger MOD_DIVISOR = new BigInteger("97");

    public IBAN(String iban){
        this.iban = iban.replace(" ", "").toUpperCase();
    }

    public boolean isValid(){
        return hasGoodCharacters()
                && isLengthCorrect()
                && isModGood();
    }

    @Override
    public String toString() {
        return iban;
    }

    private String getCountryCode() {
        return iban.substring(0,2);
    }

    private Integer getLength() {
        return iban.length();
    }

    private IBAN getWithReplacedChars(){
        return new IBAN(iban.substring(4) + iban.substring(0, 4));
    }

    private BigInteger asNumber(){
        StringBuilder sb = new StringBuilder();
        for(char c : iban.toCharArray()){
            if(CharUtil.isNumeric(c))
                sb.append(c);
            else
                sb.append(CharUtil.letterToNumber(c));
        }
        return new BigInteger(sb.toString());
    }

    private boolean hasGoodCharacters(){
        return iban
                .chars()
                .filter(c -> !Character.isDigit(c) && !Character.isUpperCase(c))
                .count() == 0;
    }

    private boolean isLengthCorrect(){
        if(iban.length() < 3)
            return false;

        Integer length = CountryList.get().get(this.getCountryCode());

        return length != null && length.equals(this.getLength());
    }

    private boolean isModGood(){
        IBAN replacedIban = this.getWithReplacedChars();
        BigInteger number = replacedIban.asNumber();
        return number.mod(MOD_DIVISOR).equals(BigInteger.ONE);
    }
}
