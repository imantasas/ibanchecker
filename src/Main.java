import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        if(args.length == 0){
            handleUserInput();
        }else{
            handleFile(args[0]);
        }
    }

    private static void handleUserInput() {
        String userInput;
        Scanner scanner = new Scanner(System.in);
        do{
            System.out.println("Please type in an IBAN or type \"quit\" to exit the application.");
            userInput = scanner.nextLine();
            if(userInput.equals("quit"))
                break;
            System.out.println(new IBAN(userInput).isValid() ? "IBAN is valid" : "IBAN is not valid");
        }while(true);
    }

    private static void handleFile(String filePath){
        try {
            List<String> lines = Files.readAllLines(Paths.get(filePath));
            int lastIndex = filePath.lastIndexOf('.');
            String outputFile = filePath.substring(0, lastIndex) + ".out";
            FileWriter fw = new FileWriter(outputFile);
            for(String line : lines){
                fw.write(line + ";" + new IBAN(line).isValid() + "\n");
            }
            fw.close();
        } catch (IOException e) {
            System.out.println("Error while working with the files");
        }
    }
}
