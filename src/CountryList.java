import java.util.*;

public class CountryList {
    private static Map<String, Integer> countries = new HashMap<>();

    public static Map<String, Integer> get(){
        if(countries.size() == 0)
            populate();

        return countries;
    }

    /*
        i know this is tedious, but I wasn't sure if I'm
        allowed to use external APIs to fetch this data
        (and I had troubles even finding proper ones)
    */

    private static void populate() {
        countries.put("AD", 24);
        countries.put("AL", 28);
        countries.put("AT", 20);
        countries.put("AZ", 28);
        countries.put("BH", 22);
        countries.put("BE", 16);
        countries.put("BA", 20);
        countries.put("BG", 22);
        countries.put("BY", 28);
        countries.put("BR", 29);
        countries.put("HR", 21);
        countries.put("CR", 21);
        countries.put("CY", 28);
        countries.put("CZ", 24);
        countries.put("DK", 18);
        countries.put("DO", 28);
        countries.put("EE", 20);
        countries.put("FO", 18);
        countries.put("FI", 18);
        countries.put("FR", 27);
        countries.put("GE", 22);
        countries.put("GT", 28);
        countries.put("DE", 22);
        countries.put("GI", 23);
        countries.put("GB", 22);
        countries.put("GR", 27);
        countries.put("GL", 18);
        countries.put("HU", 28);
        countries.put("IS", 26);
        countries.put("IE", 22);
        countries.put("IM", 22);
        countries.put("IQ", 23);
        countries.put("IT", 27);
        countries.put("IR", 26);
        countries.put("IL", 23);
        countries.put("JE", 22);
        countries.put("JO", 30);
        countries.put("KZ", 20);
        countries.put("KW", 30);
        countries.put("XK", 20);
        countries.put("LV", 28);
        countries.put("LI", 21);
        countries.put("LT", 20);
        countries.put("LU", 20);
        countries.put("MK", 19);
        countries.put("MT", 31);
        countries.put("MD", 24);
        countries.put("MC", 27);
        countries.put("ME", 22);
        countries.put("MR", 27);
        countries.put("MU", 30);
        countries.put("NL", 18);
        countries.put("NO", 15);
        countries.put("PS", 29);
        countries.put("PL", 28);
        countries.put("PT", 25);
        countries.put("PK", 24);
        countries.put("QA", 29);
        countries.put("RO", 24);
        countries.put("SM", 27);
        countries.put("SA", 24);
        countries.put("SK", 24);
        countries.put("SI", 19);
        countries.put("ST", 25);
        countries.put("ES", 24);
        countries.put("SE", 24);
        countries.put("CH", 21);
        countries.put("TN", 24);
        countries.put("TR", 26);
        countries.put("AE", 23);
        countries.put("RS", 22);
        countries.put("SC", 31);
        countries.put("LC", 32);
        countries.put("UA", 29);
        countries.put("VA", 22);
        countries.put("VG", 24);
        countries.put("TL", 23);
    }
}
