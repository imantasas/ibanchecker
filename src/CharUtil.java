public class CharUtil {

    public static boolean isNumeric(char c){
        return c >= 48 && c <= 57;
    }

    public static int letterToNumber(char c){
        return Character.toUpperCase(c) - 55;
    }

}
